var video_status=1; //video is on
var decodeinfo_copy;
var decodedInformation;
var scanning_status=0;
var candi_name="";

function tick() {
    var video                   = document.getElementById("video-preview");
    var qrCanvasElement         = document.getElementById("qr-canvas");
    var qrCanvas                = qrCanvasElement.getContext("2d");
    var width, height;

    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        qrCanvasElement.height  = video.videoHeight;
        qrCanvasElement.width   = video.videoWidth;
        qrCanvas.drawImage(video, 0, 0, qrCanvasElement.width, qrCanvasElement.height);
        try {
        var result = qrcode.decode();
        console.log(result);
        document.getElementById("text_para1").innerHTML= ("Decoded Information:"+ result);
        /* Video can now be stopped */
        if (result.length > 3){
        video.pause();
        video.src = "";
        video.srcObject.getVideoTracks().forEach(track => track.stop());
        video_status=0; // video off

        /* Display Canvas and hide video stream */
        qrCanvasElement.classList.remove("hidden");
        video.classList.add("hidden");}
        } 
        catch(e) {
        /* No Op */
        }
    }

    /* If no QR could be decoded from image copied in canvas */
    if (!video.classList.contains("hidden"))
        setTimeout(tick, 100);
}

function tick2() {
    var video                   = document.getElementById("video-preview2");
    var qrCanvasElement         = document.getElementById("qr-canvas2");
    var qrCanvas                = qrCanvasElement.getContext("2d");
    var width, height;

    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        qrCanvasElement.height  = video.videoHeight;
        qrCanvasElement.width   = video.videoWidth;
        qrCanvas.drawImage(video, 0, 0, qrCanvasElement.width, qrCanvasElement.height);
        try {
        var result = qrcode.decode();
        console.log(result);
        document.getElementById("text_para4").innerHTML= ("Decoded Information:"+ result);
        /* Video can now be stopped */
        if (result.length > 3){
        video.pause();
        video.src = "";
        video.srcObject.getVideoTracks().forEach(track => track.stop());
        video_status=0; // video off

        /* Display Canvas and hide video stream */
        qrCanvasElement.classList.remove("hidden");
        video.classList.add("hidden");}
        } 
        catch(e) {
        /* No Op */
        }
    }

    /* If no QR could be decoded from image copied in canvas */
    if (!video.classList.contains("hidden"))
        setTimeout(tick2, 100);
}


function decodeImageFromBase64(data, callback){
    // set callback
    qrcode.callback = callback;
    // Start decoding
    qrcode.decode(data)
}
    
    var openFile = function(event) {
    var input = event.target; 
    var reader = new FileReader(); 
    //var canvas= document.getElementById("qr-code");
    //var reader = canvas.toDataURL("image/png")//.replace("image/png", "image/octet-stream");
    console.log(reader);
    reader.onload = function(){
        var dataURL = reader.result;  console.log(dataURL);
        decodeImageFromBase64(dataURL,function(decodedInformation){
        console.log(decodedInformation);
        if (scanning_status==0){
            candi_name=decodedInformation;
            //document.getElementById("text_para2").innerHTML= "Decoded Information:"+"\n"+decodedInformation;
            scanning_status=1;
            for (h=0;h<100;h++){}
            if (decodedInformation.length>10){alert("your id is recorded");
            document.getElementById('p_description').innerHTML="";
            document.getElementById('p_description').innerHTML="Please scan or load the survey name."
            //aaa();
            //read_survey();
            }
        }
        else{
            //candi_name=decodedInformation;
            //document.getElementById("text_para2").innerHTML= "Decoded Information:"+"\n"+decodedInformation;
            //scanning_status=;
            for (h=0;h<100;h++){}
            if (decodedInformation.length>10){alert("your test id is recorded");
            load_next_content();
            //read_survey();
            }
        }
            
        
            
        
        });
        // var result=qrcode.decode(dataURL);  console.log(result);
        //var output = document.getElementById('output'); console.log(output.value);
        output.src = dataURL; 
    };
    reader.readAsDataURL(input.files[0]);
   // document.getElementById("text_para2").innerHTML= ("decodedInformation:"+ decodedInformation);


};

function read_survey(){
    document.getElementById('sur_cam').innerHTML="";
    document.getElementById('sur_cam2').style.visibility='visible';
    document.getElementById('b2').innerHTML = document.getElementById('sur_cam2').value="";
}

function load_next_content(){
    //console.log("okay");
    document.getElementById('sur_cam').innerHTML="";
    document.getElementById('load_survey1').style.visibility='visible';
    document.getElementById('b2').innerHTML = document.getElementById('load_survey1').value="";
    
}

function Stopcam(){
    var video = document.getElementById("video-preview");
    video.pause();   
    video.src = "";
    video.srcObject.getVideoTracks().forEach(track => track.stop());
    video.classList.add("hidden");
}

function Startcam(){
   // window.onload =  function() {
        /* Ask for "environnement" (rear) camera if available (mobile), will fallback to only available otherwise (desktop).
         * User will be prompted if (s)he allows camera to be started */
        if (video_status==0){ 
            video.srcObject.getVideoTracks().forEach(track => track.start());
            video.classList.add("visible");
            video_status=1   
        }
        navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" }, audio: false }).then(function(stream) {
          var video = document.getElementById("video-preview");
          video.srcObject = stream;
          video.setAttribute("playsinline", true); /* otherwise iOS safari starts fullscreen */
          video.play();
          setTimeout(tick, 100); /* We launch the tick function 100ms later (see next step) */
        })
        .catch(function(err) {
          console.log(err); /* User probably refused to grant access*/
        });
    //}
}