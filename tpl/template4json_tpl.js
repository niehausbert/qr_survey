// JSON2Schema
// File for ID 'template_id': jsoneditor_app/tpl/template4json_tpl.js
// created with JSON2Schema: https://niehausbert.gitlab.io/JSON2Schema

vDataJSON.tpl.template4json =  `

  <!-- Object: root --> 
*  
  <!-- Array: root.Question_Bank --> 
<!-- Array Path: root.Question_Bank  -->
{{#each Question_Bank}}
* Sub-Type of Array Element: 'object'
  <!-- Object: root.Question_Bank.* --> 
{{#with this}}
*  
  <!-- Array: root.Question_Bank.*.Questions --> 
<!-- Array Path: root.Question_Bank.*.Questions  -->
  <!-- Array: root.Question_Bank.*.Questions --> 
{{/with}}
  <!-- Object: root.Question_Bank.* --> 
{{/each}}
  <!-- Array: root.Question_Bank --> 
*  {{{QR_code}}}
 <!-- String Format: text --> 
  <!-- Object: root -->`;
