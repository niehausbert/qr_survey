**Survey with QR generator and scanner**  
Survey with QR generator and scanner  is an open source software product with an HTML/JS demonstrator that, as a web-based application in a browser. This application is used to create surveys  and collect the data of those surveys from different users and store them in local storage.  Each questionnaire has a QR code and each user is given a unique id ie QR code. The Questionnaire and the collected data are made available as an output file in JSON format. The application is able to run alone in the browser without an Internet connection.
The WebApp is started as an HTML file locally with a browser on the computer. The landing page gives you a questionnaire edit page where questions can be upload and saved to a JSON file. There is another tab with a video icon where you can create a QR code or take survey if you already have a QR code. You can either scan through webcam or upload a image of your QR code and the QR code of the Questionnaire. Once the survey is submitted, the data is stored in local storage of the browser. Later you can take the survey data as JSON output file using FileSaver.js

**Installation:**

On the client side, no installation is necessary. You just point your browser to the URL of your deployment.
To run application with localhost server setup.
1.	Install Node.js https://nodejs.org/en/download/
2.	In cmd check npm(node package manage) is configured or not with npm --version
3.	Download npm package called http-server. npm install -g http-server
4.	In terminal go to project path and just run cmd: - http-server
5.	In response you will get list of localhost urls, use any one to head towards application. (ex: http://127.0.0.1:8080/index.html)

**Usage:**

•	The Edit tab has the option to create a questionnaire. Questions can have different mode for answer options ie checkbox, radio button, dropdown, comment, Boolean etc.

•	The QR generation tab takes in 4 random numbers to create a unique QR code for the user.

•	The tab with video icon is where QR code of the user and the questionnaire is uploaded to take the survey.

•	Once the survey is done click the "submit" button to store data to local storage and if required click "export to JSON" button to generate a JSON file with stored data.

**Version:**

This is the first version being deployed "v1".

**Development Platform:**

The Webapp was developed and tested on windows 10 platform.

**Working Environment:**

The webapp works with Google chrome and Mozilla Firefox. Internet connection is not required to run this webapp i.e., it also works offline.

**Libraries:**

This make use of four main JavaScript libraries:

1] QR Code Generator:The offline QR-Code generation can be performed with the [`qrcode-generator`](https://kazuhikoarase.github.io/qrcode-generator/js/demo/) by [Kazuhiko Arase](https://www.github.com/kazuhikoarase). 

2] QR Code Scanner [`jsqrcode`](https://kazuhikoarase.github.io/qrcode-generator/js/demo/) by [Lazar Laszlo](https://www.github.com/LazarSoft) that is able to decode the URL or the text content from a provided QR Code in the WebCam. The QR-Code scanner run also completely in the web browser.

3] The questionnaire is generated with [SurveyJS](http://surveyjs.org/). The package [SurveyJS] is OpenSource and matches with the [Open Community Approach](https://en.wikiversity.org/wiki/Open_Community_Approach) for learning resources in [Wikiversity](https://en.wikiversity.org/wiki/Open_Educational_Resources). The [SurveyJS library] is also used to display the questionnaire in the browser. The interaction of the users with the questionnaire populate a JSON database record that represents a single questionnaire.

4] FileSaver.js (https://github.com/eligrey/FileSaver.js) is a library to store data on the local computer (via the download functionality of browsers) in a WebApp. Therefore, no implementation of a server application is necessary.

**Execution file:**

The file that has to be executed to run the webapp is "/index.html"

**Input:**

The user can give text input to create a questionnaire and can scan the QR code through webcam or upload a image file of the QR code (unique identity and QR code of questionnaire) in the webapp.

**Output:**

The webapp is able to create a questionnaire with a QR code for the same and save the questions into a JSON file stored locally. It also generate a QR code as unique identity of the user. The Survey data is stored in local storage which can later be put into JSON file by clicking the button "export data".
